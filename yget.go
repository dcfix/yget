package main

import (
	"io"
	"net/http"
	"os"
        "fmt"
)

func init() {
  if len(os.Args) != 2 {
    fmt.Println("Usage: yget.exe <url>")
    os.Exit(-1)
  }
}

func main() {

  r, err := http.Get(os.Args[1])
  if err != nil {
    fmt.Println(err)
    return
  }
  // io.Copy copies from the body to the stdout
  io.Copy(os.Stdout, r.Body)
  if err = r.Body.Close(); err != nil {
    fmt.Println(err)
  }
}



